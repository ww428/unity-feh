﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Feh.Samples
{
    public enum Footballer
    {
        MESSI,
        RONALDO,
        NEYMAR,
        SAKA,
        SMITH_ROWE,
        MARTINELLI
    }
}

