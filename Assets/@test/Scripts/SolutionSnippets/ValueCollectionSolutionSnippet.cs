﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Utilities;

namespace Feh.Samples
{
    [CreateAssetMenu(menuName = "Test/Sokola Rimba 3/ValueCollectionSolutionSnippet", fileName = "ValueCollectionSolutionSnippet")]
    public class ValueCollectionSolutionSnippet : ScriptableObject
    {
        // arrays and list can be null (doesn't refer to anything. TL;DR unusable until you set it)
        // for more info please see: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/null

        // an array's length (size, number of elements inside it) is fixed once we initialize it
        // we can change its contents but not add/remove elements from it
        public Footballer[] m_FootballerArray = new Footballer[0];

        // a list's count (essentially same as array's length) can increase or decrease
        // we can change its contents AND add/remove elements from it
        public List<Footballer> m_FootballerList = new List<Footballer>();

        public Footballer m_SelectedFootballer;

        [Button]
        public void DebugArrayContents()
        {
            var footballerArray = m_FootballerArray;

            if (footballerArray == null)
            {
                Debug.LogError("Footballer array is null");
                return;
            }

            // length -> number of footballers in the array
            if (footballerArray.Length == 0)
            {
                Debug.LogError("Footballer array is empty");
                return;
            }

            string message = $"Footballer array length: {footballerArray.Length}. ";

            // arrays and lists are indexed with integers starting from 0
            for (int i = 0; i < footballerArray.Length; i++)
            {
                // if i == 0, footballerArray[0] means -> look into the first element of footballerArray
                message += footballerArray[i];

                if (i < footballerArray.Length - 1)
                    message += ", ";
            }

            Debug.Log(message);
        }

        [Button]
        public void DebugListContents()
        {
            var footballerList = m_FootballerList;

            // this is same as m_FootballerList == null || m_FootballerList.Count == 0
            if (footballerList.IsNullOrEmpty())
            {
                Debug.LogError("Footballer list is null or empty");
                return;
            }

            string message = $"Footballer list count: {footballerList.Count}. ";

            for (int i = 0; i < footballerList.Count; i++)
            {
                message += footballerList[i];

                if (i < footballerList.Count - 1)
                    message += ", ";
            }

            Debug.Log(message);

            // also note how array.Length and list.Count are reused in the functions
            // yes can we store them in local variables if we want, but it's usually not a big deal in these cases
        }
        
        [Button]
        public void Debug_IsSelectedFootballerInArray()
        {
            var footballer = m_SelectedFootballer;
            var footballerArray = m_FootballerArray;

            bool isInArray = false;
            
            for (int i = 0; i < footballerArray.Length; i++)
            {
                if (footballerArray[i] == footballer)
                {
                    isInArray = true;

                    // break tells the program to stop looking into the array
                    // if the third footballer is already the one selected, then we don't need to look into the fourth footballer and so on :)
                    break;
                }
            }

            Debug.Log($"Is footballer {footballer} in array? {isInArray}");
        }

        [Button]
        public void Debug_IsSelectedFootballerInList()
        {
            var footballer = m_SelectedFootballer;

            // it's faster to find in a list :)
            bool isInList = m_FootballerList.Contains(footballer);
            Debug.Log($"Is footballer {footballer} in list? {isInList}");
        }

        [Button]
        public void Debug_AddSelectedFootballerToList()
        {
            var footballer = m_SelectedFootballer;

            // NOTE: lists do allow for duplicates
            m_FootballerList.Add(m_SelectedFootballer);
            Debug.Log($"Selected footballer {footballer} added to list");
        }

        [Button]
        public void Debug_RemoveSelectedFootballerFromList()
        {
            var footballer = m_SelectedFootballer;
            // if selected footballer is not in the list, this function does nothing
            bool isRemoved = m_FootballerList.Remove(footballer);
            Debug.Log($"Is footballer {footballer} removed? : {isRemoved}");
        }

        [Button]
        public void TestNullReferenceException()
        {
            // a null reference exception should appear here :)
            List<int> testList = null;
            testList.Add(0);
        }

        [Button]
        public void TestOutOfRangeException()
        {
            List<int> testList = new List<int>();
            testList.Add(0);
            testList.Add(1);

            // the valid options are testList[0] and testList[1] because the list only has 2 elements
            Debug.Log($"Let's try out of range exception :), {testList[2]}");
        }
    }

}

