﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Feh.Samples
{
    [CreateAssetMenu(menuName = "Test/Sokola Rimba 1/SampleScriptableObject", fileName = "SampleScriptableObject")]
    public class SampleScriptableObject : ScriptableObject
    {
        public int m_TestInt;
        public float m_TestFloat;
        public string m_TestString;
        public bool m_TestBool;

        [Button]
        private void HelloWorld()
        {
            // Console.WriteLine("Hello World");
            Debug.Log("Hello World");
        }

        [Button]
        private void IntPlusFloat()
        {
            float result = m_TestInt + m_TestFloat;
            Debug.Log($"test int {m_TestInt} + test float {m_TestFloat} = {result}");
        }

        [Button]
        private void IntMultiplyByFloat()
        {
            float result = m_TestInt * m_TestFloat;
            Debug.Log($"test int {m_TestInt} * test float {m_TestFloat} = {result}");
        }

        [Button]
        private void IntDivideByFloat()
        {
            float result = m_TestInt / m_TestFloat;
            Debug.Log($"test int {m_TestInt} / test float {m_TestFloat} = {result}");
        }

        [Button]
        private void LogString()
        {
            Debug.Log($"Test string: {m_TestString}");
        }

        [Button]
        private void BoolTrueOrFalse()
        {
            Debug.Log($"Test bool: {m_TestBool}");
        }
    }
}


