﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Feh.Samples
{
    [CreateAssetMenu(menuName = "Test/Sokola Rimba 2/EnumSolutionSnippet", fileName = "EnumSolutionSnippet")]
    public class EnumSolutionSnippet : ScriptableObject
    {
        public int m_CostMessi;
        public int m_CostRonaldo;
        public int m_CostNeymar;

        public Footballer m_SelectedFootballerForDebug;

        [Button]
        public void DebugSelectedFootballer()
        {
            // Question: Why is a local reference used here?
            var footballer = m_SelectedFootballerForDebug;
            Debug.Log($"Selected Footballer: {footballer}. Transfer fee: {GetTransferFee_If(footballer)}");
        }

        private int GetTransferFee_If(Footballer footballer)
        {
            // functions end on return X (in case of void functions that don't return anything, just write "return;")
            if (footballer == Footballer.MESSI)
                return m_CostMessi;
            if (footballer == Footballer.RONALDO)
                return m_CostRonaldo;
            if (footballer == Footballer.NEYMAR)
                return m_CostNeymar;

            // Some thing for Sokola Rimbas in a month or so
            // Exceptions are thrown when things just don't work as they should
            // So if somehow there is a footballer that's not among those 3 above (maybe if Haaland is added? :) )
            // It's not handled, so we throw an exception
            // Also feel free to ask for an Indonesian explanation :)
            throw new System.ArgumentException($"Unhandled footballer transfer fee");
        }

        private int GetTransferFee_Switch(Footballer footballer)
        {
            switch (footballer)
            {
                case Footballer.MESSI: return m_CostMessi;
                case Footballer.RONALDO: return m_CostRonaldo;
                case Footballer.NEYMAR: return m_CostNeymar;
                default: throw new System.ArgumentException($"Unhandled footballer transfer fee");
            }
        }
    }
}


