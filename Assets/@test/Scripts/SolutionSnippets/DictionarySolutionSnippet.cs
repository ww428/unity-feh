﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Feh.Samples
{
    [CreateAssetMenu(menuName = "Test/Sokola Rimba 4/DictionarySolutionSnippet", fileName = "DictionarySolutionSnippet")]
    public class DictionarySolutionSnippet : SerializedScriptableObject
    {
        // Footballer -> key, int -> value
        // in this case the value should be  the transfer fee
        public Dictionary<Footballer, int> m_FootballerTransferFees;
        public List<Footballer> m_FootballerPurchaseCart;
        public Footballer m_SelectedFootballer;
        public int m_TransferFeeForSelectedFootballer;

        [Button]
        public void DebugSelectedFootballerTransferFee_WithContainsKey()
        {
            // Some note with declaration of "var" instead of "Footballer"
            // Not advised to do this with primitive variables such as int, float, string, etc
            var selectedFootballer = m_SelectedFootballer;
            if (m_FootballerTransferFees.ContainsKey(selectedFootballer))
                // m_FootballerTransferFees[selectedFootballer] -> this directly looks for the transfer fee with indexing based on Footballer
                Debug.Log($"Selected footballer {selectedFootballer} transfer fee: {m_FootballerTransferFees[selectedFootballer]}");    
            else
                Debug.Log($"Selected footballer {selectedFootballer} is not present in footballer transfer fees dictionary");
        }

        [Button]
        public void DebugSelectedFootballerTransferFee_WithTryGet()
        {
            int transferFee = 0;
            var selectedFootballer = m_SelectedFootballer;

            // try get value: returns true (and also modifies transferFee) if the selected footballer is in the dictionary
            // returns false and does not modify transferFee otherwise
            if (m_FootballerTransferFees.TryGetValue(selectedFootballer, out transferFee))
                Debug.Log($"Selected footballer {selectedFootballer} transfer fee: {transferFee}");
            else
                Debug.Log($"Selected footballer {selectedFootballer} is not present in footballer transfer fees dictionary");
        }

        [Button]
        public void Debug_AddOrReplaceSelectedFootballerToDictionary()
        {
            // will replace previous value if already exists
            // you can prevent this by checking if m_FootballerTransferFees.ContainsKey(selectedFootballer)
            m_FootballerTransferFees[m_SelectedFootballer] = m_TransferFeeForSelectedFootballer;
        }

        [Button]
        public void Debug_AddSelectedFootballerToDictionary()
        {
            // this will fail if the dictionary already contains KvP for selected footballer
            m_FootballerTransferFees.Add(m_SelectedFootballer, m_TransferFeeForSelectedFootballer);
        }

        [Button]
        public void Debug_RemoveSelectedFootballerFromDictionary()
        {
            // very similar to List.Remove()
            m_FootballerTransferFees.Remove(m_SelectedFootballer);
        }

        [Button]
        public void DebugTotalFootballerTransferFeesInDictionary()
        {
            int totalTransferFee = 0;

            foreach (var transferFeeKvp in m_FootballerTransferFees)
            {
                // transferFeeKvp.Key will give you the footballer
                totalTransferFee += transferFeeKvp.Value;
            }

            Debug.Log($"Total transfer fee: {totalTransferFee}");
        }

        [Button]
        public void DebugForListDuplicates()
        {
            // this probably should be in value collection snippet, but nevermind

            var cart = m_FootballerPurchaseCart;
            var uniqueFootballers = new List<Footballer>();
            var dupeFootballers = new List<Footballer>();

            for (int i = 0; i < cart.Count; i++)
            {
                if (uniqueFootballers.Contains(cart[i]))
                {
                    uniqueFootballers.Remove(cart[i]);
                    dupeFootballers.Add(cart[i]);
                }
                else
                {
                    uniqueFootballers.Add(cart[i]);
                }
            }

            // can confirm there's at least 1 dupe
            // you can also debug whom exactly are the dupes from dupeFootballers
            if (dupeFootballers.Count > 0)
                Debug.Log("Has dupe footballers in cart");
            else
                Debug.Log("No dupe footballers in cart");
        }

        [Button]
        public void DebugListContentAgainstDictionary()
        {
            // in this case we want to see if all of the footballers in the cart are also in the dictionary
            // because it doesn't make sense if you want to buy a player that's not for sale by the selling club :)
            bool hasUnlistedPlayer = false;

            var cart = m_FootballerPurchaseCart;

            for (int i = 0; i < cart.Count; i++)
            {
                if (m_FootballerTransferFees.ContainsKey(cart[i]))
                    continue;

                hasUnlistedPlayer = true;
                break;
            }

            if (hasUnlistedPlayer)
                Debug.Log("At least 1 footballer in cart is not listed for transfer");
            else
                Debug.Log("All footballers in cart are listed for transfer");
        }
    }
}


