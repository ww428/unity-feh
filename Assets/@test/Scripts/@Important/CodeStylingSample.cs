﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Feh.Samples
{
    public class CodeStylingSample
    {
        // -------- public variables first beginning with "m_" (member) prefix
        public int m_MyInt;
        public string m_MyString;

        // -------- private vars after public vars
        private int m_MyFloat;

        // -------- public functions first
        // it's okay for function params to be unused in certain cases
        public void MyPublicFunction(int functionParam)
        {
            // your IDE (vstudio/vscode/jetbrains rider) should provide a warning on this
            // please ensure your code does not generate any such warnings like this
            // because 1. it's unclean, 2. lots of warnings will slow down recompile time 
            // PPC repo is also guilty of this but recently we refactored to remove a lot of these warnings
            int myLocalVariable = 1;
        }

        // -------- private functions later
        private void MyPrivateFunction()
        {

        }
    }
}


